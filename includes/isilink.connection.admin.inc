<?php

class ISILinkConnectionUIController extends EntityDefaultUIController {
	
	public function __construct($entity_type, $entity_info) {
    $this->entityType = $entity_type;
    $this->entityInfo = $entity_info;
    $this->path = $this->entityInfo['admin ui']['path'];
    $this->statusKey = empty($this->entityInfo['entity keys']['active']) ? 'active' : $this->entityInfo['entity keys']['active'];
  }
	
	/**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {

    $items = parent::hook_menu();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%entity_object';

    $items[$this->path . '/add']['title'] = t('Add new ISILink connection');
		$items[$this->path . '/add']['weight'] = -5;
		$items[$this->path . '/add']['file'] = 'isilink.connection.admin.inc';
		$items[$this->path . '/add']['file path'] = drupal_get_path('module', $this->entityInfo['module']) . '/includes/';
		
		$items[$this->path . '/manage/' . $wildcard . '/edit']['title'] = t('Edit ISILink connection');
		$items[$this->path . '/manage/' . $wildcard . '/edit']['weight'] = -5;
		$items[$this->path . '/manage/' . $wildcard . '/edit']['file'] = 'isilink.connection.admin.inc';
		$items[$this->path . '/manage/' . $wildcard . '/edit']['file path'] = drupal_get_path('module', $this->entityInfo['module']) . '/includes/';
		
    $items[$this->path . '/manage/' . $wildcard . '/activate'] = array(
			'title' => 'Activate',
			'page callback' => 'drupal_get_form',
			'page arguments' => array('isilink_activate_connection_form', $id_count + 1),
			'access arguments' => array('access administration pages'),
			'file' => 'isilink.connection.admin.inc',
			'file path' => drupal_get_path('module', $this->entityInfo['module']) . '/includes/',
			'type' => MENU_CALLBACK,
    );

    $items[$this->path . '/manage/' . $wildcard . '/deactivate'] = array(
			'title' => 'Dectivate',
			'page callback' => 'isilink_deactivate_connection',
			'page arguments' => array($id_count + 1),
			'access arguments' => array('access administration pages'),
			'file' => 'isilink.connection.admin.inc',
			'file path' => drupal_get_path('module', $this->entityInfo['module']) . '/includes/',
			'type' => MENU_CALLBACK,
    );
		
    $items[$this->path . '/manage/' . $wildcard . '/delete'] = array(
			'title' => 'Delete',
			'page callback' => 'isilink_connection_delete_form_wrapper',
			'page arguments' => array($id_count + 1),
			'access arguments' => array('access administration pages'),
			'type' => MENU_LOCAL_TASK,
			'context' => MENU_CONTEXT_INLINE,
			'weight' => 10,
			'file' => 'isilink.connection.admin.inc',
			'file path' => drupal_get_path('module', $this->entityInfo['module']) . '/includes/',
    );

    return $items;
  }
	
	/**
   * Generates the render array for a overview table for arbitrary entities
   * matching the given conditions.
   *
   * @param $conditions
   *   An array of conditions as needed by entity_load().

   * @return Array
   *   A renderable array.
   */
  public function overviewTable($conditions = array()) {
    $entities = entity_load($this->entityType, FALSE, $conditions);
    ksort($entities);

    $rows = array();
    foreach ($entities as $entity) {
      $rows[] = $this->overviewTableRow($conditions, entity_id($this->entityType, $entity), $entity);
    }
    // Assemble the right table header.
    $header = array(t('Label'));
    /*if (!empty($this->entityInfo['exportable'])) {
      $header[] = t('Status');
    }*/
		
		$header[] = t('Active');
		
    // Add operations with the right colspan.
    $field_ui = !empty($this->entityInfo['bundle of']) && module_exists('field_ui');
    $exportable = !empty($this->entityInfo['exportable']);
    $colspan = 3;
    $colspan = $field_ui ? $colspan + 2 : $colspan;
    $colspan = $exportable ? $colspan + 1 : $colspan;
    $header[] = array('data' => t('Operations'), 'colspan' => $colspan);

    $render = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('None.'),
    );
    return $render;
  }
	
	/**
   * Generates the row for the passed entity and may be overridden in order to
   * customize the rows.
   *
   * @param $additional_cols
   *   Additional columns to be added after the entity label column.
   */
  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $entity_uri = entity_uri($this->entityType, $entity);

    $row[] = array('data' => array(
      '#theme' => 'entity_ui_overview_item',
      '#label' => entity_label($this->entityType, $entity),
      '#name' => !empty($this->entityInfo['exportable']) ? entity_id($this->entityType, $entity) : FALSE,
      '#url' => $entity_uri ? $entity_uri : FALSE,
      '#entity_type' => $this->entityType),
    );

    // Add in any passed additional cols.
    foreach ($additional_cols as $col) {
      $row[] = $col;
    }

    // Add a row for the exportable status.
    /*if (!empty($this->entityInfo['exportable'])) {
      $row[] = array('data' => array(
        '#theme' => 'entity_status',
        '#status' => $entity->{$this->statusKey},
      ));
    }*/
		
		$row[] = $entity->active ? 'Yes' : 'No';
		
    // In case this is a bundle, we add links to the field ui tabs.
    $field_ui = !empty($this->entityInfo['bundle of']) && module_exists('field_ui');
    // For exportable entities we add an export link.
    $exportable = !empty($this->entityInfo['exportable']);
    $colspan = 3;
    $colspan = $field_ui ? $colspan + 2 : $colspan;
    $colspan = $exportable ? $colspan + 1 : $colspan;

    // Add operations depending on the status.
    if (entity_has_status($this->entityType, $entity, ENTITY_FIXED)) {
      $row[] = array('data' => l(t('clone'), $this->path . '/manage/' . $id . '/clone'), 'colspan' => $colspan);
    }
    else {
      $row[] = l(t('edit'), $this->path . '/manage/' . $id . '/edit');

      if ($field_ui) {
        $row[] = l(t('manage fields'), $this->path . '/manage/' . $id . '/fields');
        $row[] = l(t('manage display'), $this->path . '/manage/' . $id . '/display');
      }

      $row[] = l(t('clone'), $this->path . '/manage/' . $id . '/clone');
      if (empty($this->entityInfo['exportable']) || !entity_has_status($this->entityType, $entity, ENTITY_IN_CODE)) {
        $row[] = l(t('delete'), $this->path . '/manage/' . $id . '/delete', array('query' => drupal_get_destination()));
      }
      elseif (entity_has_status($this->entityType, $entity, ENTITY_OVERRIDDEN)) {
        $row[] = l(t('revert'), $this->path . '/manage/' . $id . '/revert', array('query' => drupal_get_destination()));
      }
      else {
        $row[] = '';
      }
    }
    if ($exportable) {
      $row[] = l(t('export'), $this->path . '/manage/' . $id . '/export');
    }
    return $row;
	}
}

function isilink_connection_page() {
	return ' ';
}

function isilink_connection_form($form, &$form_state, $connection, $op = 'view') {
  switch ($op) {
    case 'add':
    case 'edit':
      return isilink_connection_edit_form($form, $form_state, $connection);
  }
}

function isilink_connection_edit_form($form, &$form_state, $connection) {
  $form['connection_id'] = array('#type' => 'value', '#value' => $connection->connection_id ? $connection->connection_id : '');
 
  $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Connection Name'),
      '#description' => t('Name to identify the connection in the administration pages.'),
      '#required' => TRUE,
      '#size' => 80,
      '#default_value' => isset($connection->name) ? $connection->name : '',
  );
	
  $form['connection_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Connection URL'),
      '#default_value' => isset($connection->connection_url) ? $connection->connection_url : '',
      '#description' => t('Connection URL to ISILink. Typically a link to a WSDL file.'),
      '#required' => TRUE,
      '#size' => 80,
  );
	
  $form['struserid'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => isset($connection->struserid) ? $connection->struserid : '',
      '#size' => 40,
      '#required' => TRUE,
  );
	
  $form['strpassword'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => isset($connection->strpassword) ? $connection->strpassword : '',
      '#size' => 40,
      '#required' => TRUE,
  );
	
	$form['strcoid'] = array(
      '#type' => 'textfield',
      '#title' => t('Company ID'),
      '#default_value' => isset($connection->strcoid) ? $connection->strcoid : '',
      '#size' => 40,
			'#description' => t('Unique identifier for the management company.'),
      '#required' => TRUE,
  );
	
	$form['active'] = array(
      '#type' => 'select',
      '#title' => t('Active'),
      '#default_value' => isset($connection->active) ? $connection->active : TRUE,
			'#description' => t('Whether or not this connection is active.'),
      '#options' => array(TRUE => 'Active', FALSE => 'Not Active'),
  );
	
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Connection'),
      '#weight' => 40,
  );
	
  return $form;
}

function isilink_connection_form_submit(&$form, &$form_state) {
  $isilink_connection = entity_ui_form_submit_build_entity($form, $form_state);
  $isilink_connection->save();
  $form_state['redirect'] = 'admin/isilink/connection';
}

function isilink_connection_delete_form($form, &$form_state, ISILinkConnection $connection) {
  $form_state['connection'] = $connection;

  $form['#submit'][] = 'isilink_connection_delete_form_submit';

  $form = confirm_form($form, t('Are you sure you want to delete the %name connection?', array('%name' => $connection->name)), 'admin/isilink/connection', '<p>' . t('This action cannot be undone.') . '</p>', t('Delete'), t('Cancel'), 'confirm'
  );

  return $form;
}

function isilink_connection_delete_form_submit($form, &$form_state) {
  $connection = $form_state['connection'];

  $connection->delete();

  drupal_set_message(t('The %name connection has been deleted.', array('%name' => $connection->name)));
  watchdog('isilink', 'Deleted %name connection.', array('%name' => $connection->name));

  $form_state['redirect'] = 'admin/isilink/connection';
}

function isilink_connection_delete_form_wrapper(ISILinkConnection $connection = NULL) {
  return drupal_get_form('isilink_connection_delete_form', $connection);
}