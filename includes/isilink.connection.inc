<?php

function isilink_connection_entity_save(ISILinkConnection $connection) {
  return $connection->save();
}

class ISILinkConnection extends Entity {

  public $connection_id;
  public $name;
  public $connection_url;
  public $struserid;
  public $strpassword;
  public $strcoid;
  public $active;

  public function __construct($values = array()) {
    parent::__construct($values, 'isilink_connection');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  public function label() {
    return $this->name;
  }

  public function save() {
    parent::save();
  }
	
	public function delete() {
    parent::delete();
  }
}

class ISILinkConnectionController extends EntityAPIControllerExportable {

  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  public function create(array $values = array()) {
    // Add values that are specific to our Model
    $values += array(
			'connection_id' => '',
			'name' => NULL,
			'connection_url' => NULL,
			'struserid' => NULL,
			'strpassword' => NULL,
			'strcoid' => '',
			'active' => FALSE,
    );
		
    $ISILinkConnection = parent::create($values);
    return $ISILinkConnection;
  }
}