<?php 

function isilink_settings_form() {
	$form = array();
	
	$form['isilink_cron_limit'] = array(
		'#title' => 'Cron Limit',
		'#type' => 'textfield',
		'#description' => 'How many listings should cron attempt to import/update?',
		'#default_value' => variable_get('isilink_cron_limit', 100),
	);
	
	return system_settings_form($form);
}